#!/bin/bash

echo "Start writing .env"

echo BIND_HOST=0.0.0.0 >.env
echo GRPC_BIND_PORT=8081 >>.env
echo MONGO_URL="mongodb://mongo/bintra?directConnection=true" >>.env
echo MONGO_USERNAME=muser >>.env
echo MONGO_PASSWORD=mpwd >>.env
echo MONGO_HOSTNAME=mongo >>.env
echo MONGO_PORT=27017 >>.env
echo MONGO_DB=bintratest >>.env
echo JWT_SECRET=SomeSecret >>.env
echo JWT_ISSUER=127.0.0.1 >>.env
echo MQTT_PROTO=mqtt >>.env
echo MQTT_HOSTNAME=mqtt >>.env
echo MQTT_USERNAME= >>.env
echo MQTT_PASSWORD= >>.env
echo REDIS_HOSTNAME=redis >>.env
echo MEMCACHED_LIMIT_HOST=memcached >>.env
echo MEMCACHED_LIMIT_PFX=rlbintra: >>.env
echo MEMCACHED_LIMIT_MAX=1000 >>.env
echo BUSY_LAG=150 >>.env
echo BUSY_INTERVAL=500 >>.env
echo LOGLEVEL=debug >>.env

git rev-parse HEAD >.gitrevision
ls -la .
cat .gitrevision
