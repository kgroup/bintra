/**
 * Some helper functions/utilities for creating mongodb connect url.
 * @module conf
 * @license MIT
 * @author Kai KRETSCHMANN <kai@kretschmann.consulting>
 */

const log4js = require('log4js');
const logger = log4js.getLogger();
logger.level = process.env.LOGLEVEL || 'warn';

require('custom-env').env(true);

const urlPrefix = 'mongodb://';

/**
 * @method
 * Helper function for creating mongodb connect url.
 * @returns (string) mongoUrl
 *
 */
module.exports.computeDBurl = function () {
  let tmpUrl = '';

  if (process.env.MONGO_URL) {
    tmpUrl = process.env.MONGO_URL; // give full fixed URI as option
    logger.debug('Using MONGO_URL');
  } else if (!process.env.MONGO_DB) {
    logger.error('Neither MONGO_URL nor MONGO_DB defined, FATAL!');
  } else if (process.env.MONGO_REPLICA) {
    tmpUrl = `${urlPrefix}${process.env.MONGO_REPLICA}/${process.env.MONGO_DB}?authSource=admin`;
    logger.debug('Using MONGO_REPLICA');
  } else if (process.env.MONGO_USERNAME) {
    tmpUrl = `${urlPrefix}${process.env.MONGO_USERNAME}:${process.env.MONGO_PASSWORD}@${process.env.MONGO_HOSTNAME}:${process.env.MONGO_PORT}/${process.env.MONGO_DB}?authSource=admin`;
    logger.debug('Using MONGO_USERNAME');
  } else {
    tmpUrl = `${urlPrefix}${process.env.MONGO_HOSTNAME}:${process.env.MONGO_PORT}/${process.env.MONGO_DB}?authSource=admin`;
    logger.debug('Using MONGO_HOSTNAME');
  }  

  return tmpUrl;
};
