'use strict';

const log4js = require('log4js');
const logger = log4js.getLogger();
logger.level = process.env.LOGLEVEL || 'info';

const getFakeId = function(idUser) {
    const strIdUser = String(idUser);
    const idLength = strIdUser.length;
    const idHalfLength = idLength / 2;
    logger.debug(`id length ${idLength}, half is ${idHalfLength}`)
    const fakeId = strIdUser.slice(-idHalfLength).concat(strIdUser.slice(idHalfLength));
    logger.debug(`change id from ${idUser} to ${fakeId}`)

    return fakeId;
};

module.exports = getFakeId;