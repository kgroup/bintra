'use strict';

var util = require('util');

const log4js = require('log4js');
const logger = log4js.getLogger();
logger.level = process.env.LOGLEVEL || 'info';
const mongoose = require('mongoose');
const computeDBurl = require('../conf').computeDBurl;

var originalLogFunction = console.log;
var originalErrorFunction = console.error;

const beforeEachCb = function() {
    var currentTest = this.currentTest;
    console.log = function captureLog() {
      var formattedMessage = util.format.apply(util, arguments);
      currentTest.consoleOutputs = (currentTest.consoleOutputs || []).concat(formattedMessage);
    };
    console.error = function captureError() {
      var formattedMessage = util.format.apply(util, arguments);
      currentTest.consoleErrors = (currentTest.consoleErrors || []).concat(formattedMessage);
    };
};

const afterEachCb = function() {
    console.log = originalLogFunction;
    console.error = originalErrorFunction;
};

const captureLogs = function() {
    if (typeof beforeEach !== 'function') {
        throw Error('Mocha was not loaded');
    }

    beforeEach(beforeEachCb);
    afterEach(afterEachCb);
};

let db = mongoose.connection;

function getDatabaseConnection () {
  return new Promise(function (resolve, reject) {
    const mongoUrl = computeDBurl();
    logger.debug(`DB used: ${mongoUrl}`);
    mongoose.set('strictQuery', true);
    db.on('error', error => {
      logger.error(`MongoDB connection error ${error}`);
      reject(error);
    });
    db.on('connecting', err => { if (err) { logger.error(err); } logger.info('connecting'); });
    db.on('connected', err => { if (err) { logger.error(err); } logger.info('DB connected'); });
    db.on('open', err => {
      if (err) { logger.error(err); }
      logger.info('DB opened');
      resolve(true);
    });
    mongoose.connect(mongoUrl, { });
    resolve(true);
  });
}

module.exports = captureLogs;
module.exports.beforeEachCb = beforeEachCb;
module.exports.afterEachCb = afterEachCb;
module.exports.getDatabaseConnection = getDatabaseConnection;
