'use strict';

/**
 * API controller for admin methods mapping.
 * @module controllers/admins
 * @license MIT
 * @author Kai KRETSCHMANN <kai@kretschmann.consulting>
 */

const utils = require('../utils/writer.js');
const eventEmitter = require('../utils/eventer').em;
const PackagesService = require('../service/PackagesService');
const UsersService = require('../service/UsersService');
const mongoose = require('mongoose');
const auth = require('../utils/auth');
const fs = require('fs');
const path = require('path');

const log4js = require('log4js');
const logger = log4js.getLogger();
logger.level = process.env.LOGLEVEL || /* istanbul ignore next */ 'warn'; // LCOV_EXCL_LINE
const EVENTNAME = 'apihit';

/**
 * Receive login data and return JWT token.
 * @function loginPost
 * @public
 */
module.exports.loginPost = function loginPost (args, res, _next) {
  const username = args.body.username;
  const password = args.body.password;
  let response;

  eventEmitter.emit('posthit', 'login');

  UsersService.checkUser(username, password)
    .then(function (useritem) {
      logger.info(`User found: ${useritem._id}`);
      const myRole = useritem.role;
      logger.info(`User has role ${myRole}`);
      const tokenString = auth.issueToken(username, myRole);
      response = {
        token: tokenString
      };
      res.writeHead(200, {
        'Content-Type': 'application/json'
      });
      return res.end(JSON.stringify(response));
    })
    .catch(function () {
      response = {
        message: 'Error: Credentials incorrect'
      };
      res.writeHead(403, {
        'Content-Type': 'application/json'
      });
      return res.end(JSON.stringify(response));
    });
};

/**
 * List all packages and variations.
 * @function listUsers
 * @public
 */
module.exports.listUsers = function listUsers (req, res, _next) {
  eventEmitter.emit(EVENTNAME, req);

  UsersService.listUsers()
    .then(function (payload) {
      utils.writeJson(res, payload, 200);
    })
    .catch(function (err) {
      utils.writeJson(res, err.message, 500);
    });
};

/**
 * Get user data of named user.
 * @function getUser
 * @public
 */
module.exports.getUser = function getUser (req, res, _next, name) {
  eventEmitter.emit(EVENTNAME, req);

  UsersService.getUser(name)
    .then(function (payload) {
      utils.writeJson(res, payload, 200);
    })
    .catch(function (err) {
      utils.writeJson(res, err.message, 400);
    });
};

/**
 * List all packages and variations.
 * @function deleteUser
 * @public
 */
module.exports.deleteUser = function deleteUser (req, res, _next, id) {
  eventEmitter.emit(EVENTNAME, req);

  UsersService.deleteUser(id)
    .then(function (payload) {
      utils.writeJson(res, payload, 200);
    })
    .catch(function (err) {
      utils.writeJson(res, err.message, 400);
    });
};

/**
 * List all packages and variations.
 * @function putUserStatus
 * @public
 */
module.exports.putUserStatus = function putUserStatus (req, res, _next, status, id) {
  eventEmitter.emit(EVENTNAME, req);

  logger.info(`putUserStatus ${id}/${status}!`);

  UsersService.putUserStatus(id, status)
    .then(function (payload) {
      utils.writeJson(res, payload, 200);
    })
    .catch(function (err) {
      utils.writeJson(res, err.message, 400);
    });
};

/**
 * List all packages and variations.
 * @function listUser
 * @public
 */
module.exports.listUser = function listUser (req, res, _next, id) {
  eventEmitter.emit(EVENTNAME, req);

  UsersService.listUser(id)
    .then(function (payload) {
      utils.writeJson(res, payload, 200);
    })
    .catch(function (err) {
      utils.writeJson(res, err.message, 400);
    });
};

/**
 * List all black listed domains.
 * @function listDomains
 * @public
 */
module.exports.listDomains = function listDomains (req, res, _next) {
  eventEmitter.emit(EVENTNAME, req);

  UsersService.listDomains()
    .then(function (payload) {
      utils.writeJson(res, payload, 200);
    })
    .catch(function (err) {
      utils.writeJson(res, err.message, 400);
    });
};

/**
 * Add a black listed domains.
 * @function addDomain
 * @public
 */
module.exports.addDomain = function listDomains (req, res, _next, name) {
  eventEmitter.emit(EVENTNAME, req);

  UsersService.addDomain(name)
    .then(function (payload) {
      utils.writeJson(res, payload, 200);
    })
    .catch(function (err) {
      utils.writeJson(res, err.message, 400);
    });
};

/**
 * Delete a black listed domains.
 * @function deleteDomain
 * @public
 */
module.exports.deleteDomain = function deleteDomain (req, res, _next, name) {
  eventEmitter.emit(EVENTNAME, req);

  UsersService.deleteDomain(name)
    .then(function (payload) {
      utils.writeText(res, payload, 200);
    })
    .catch(function (err) {
      utils.writeText(res, err.message, err.code);
    });
};

/**
 * Retreive a given domain.
 * @function checkDomain
 * @public
 */
module.exports.checkDomain = function checkDomain (req, res, _next, name) {
  eventEmitter.emit(EVENTNAME, req);

  UsersService.checkDomain(name)
    .then(function (payload) {
      if (payload == null) {
        utils.writeJson(res, {
          message: 'Not found'
        }, 404);
      } else {
        utils.writeJson(res, payload, 200);
      }
    })
    .catch(function (err) {
      utils.writeJson(res, err.message, 400);
    });
};

/**
 * Create a user.
 * @function createUser
 * @public
 */
module.exports.createUser = function createUser (req, res, _next, user) {
  eventEmitter.emit(EVENTNAME, req);

  UsersService.createUser(user)
    .then(function (payload) {
      utils.writeJson(res, payload, 200);
    })
    .catch(function (err) {
      utils.writeJson(res, err.message, 400);
    });
};

/**
 * List all packages and variations but filter.
 * @function searchPackages
 * @public
 */
module.exports.searchPackages = function searchPackages (req, res, _next, jsearch) {
  eventEmitter.emit(EVENTNAME, req);

  PackagesService.searchPackages(jsearch)
    .then(function (payload) {
      logger.error('Found search result');
      utils.writeJson(res, payload, 200);
    })
    .catch(function (err) {
      utils.writeText(res, err.message, err.code);
    });
};

/**
 * Update user data via patch.
 * @function patchUser
 * @public
 */
module.exports.patchUser = function patchUser (req, res, _next, jpatch, id) {
  logger.info('In PATCH controller');
  eventEmitter.emit(EVENTNAME, req);
  logger.info(req.headers['content-type']);

  UsersService.patchUser(id, jpatch)
    .then(function (payload) {
      utils.writeJson(res, payload, 200);
    })
    .catch(function (err) {
      utils.writeJson(res, err.message, 400);
    });
};

/**
 * Delete package with given package data.
 * @function deletePackage
 * @public
 */
module.exports.deletePackage = function deletePackage (req, res, _next, packageName, packageVersion, packageArch, packageFamily, packageHash) {
  eventEmitter.emit(EVENTNAME, req);

  PackagesService.deletePackage(packageName, packageVersion, packageArch, packageFamily, packageHash)
    .then(function (payload) {
      utils.writeText(res, payload, 200);
    })
    .catch(function (err) {
      utils.writeText(res, err.message, err.code);
    });
};

/**
 * Delete package with given package id.
 * @function deletePackageById
 * @public
 */
module.exports.deletePackageById = function deletePackageById (req, res, _next, id) {
  eventEmitter.emit(EVENTNAME, req);

  PackagesService.deletePackageById(id)
    .then(function (payload) {
      utils.writeText(res, payload, 200);
    })
    .catch(function (err) {
      utils.writeText(res, err.message, err.code);
    });
};

/**
 * Get all system version names.
 * @function getVersions
 * @public
 */
module.exports.getVersions = function getVersions (req, res, _next) {
  eventEmitter.emit(EVENTNAME, req);

  const jdata = process.versions;
  const json = require('../package.json');
  const gitrevFilename = path.join(__dirname, '../.gitrevision');
  let gitrevision = '';

  try {
    fs.accessSync(gitrevFilename, fs.constants.R_OK);
    gitrevision = fs.readFileSync(gitrevFilename, 'utf8');
  } catch (err) {
    logger.error(`gitrevision file not found at: ${gitrevFilename}`);
  }

  jdata.bintra = json.version;
  logger.debug(`bintra version found: ${jdata.bintra}`);

  jdata.gitrevision = gitrevision.trim();
  logger.debug(`git revision found: ${jdata.gitrevision}`);

  // get DB version
  const admin = new mongoose.mongo.Admin(mongoose.connection.db);
  let myinfo;
  async function doinfo () {
    myinfo = await admin.buildInfo();
  }
  doinfo()
    .then(() => {
      logger.debug(`mongo version found: ${myinfo.version}`);
      jdata.mongodb = myinfo.version;
      const payload = JSON.stringify(jdata);
      res.writeHead(200, {
        'Content-Type': 'application/json'
      });

      logger.info('return');
      return res.end(payload);
    })
    .catch(err => {
      logger.error('We have a probleme here: ' + err)
      res.writeHead(500, {
        'Content-Type': 'text/plain'
      });
      return res.end();
    });
};

/**
 * Get count per creator.
 * @function getCountPerCreator
 * @public
 **/
module.exports.getCountPerCreator = function getCountPerCreator (req, res, _next) {
  eventEmitter.emit(EVENTNAME, req);

  PackagesService.countPerCreator()
    .then(function (payload) {
      utils.writeJson(res, payload, 200);
    })
    .catch(function (err) {
      utils.writeJson(res, err.message, 400);
    });
};

/**
 * List authentication log.
 * @function getLogauth
 * @public
 */
module.exports.getLogauth = function getLogauth (req, res, _next) {
  eventEmitter.emit(EVENTNAME, req);

  UsersService.getLogauth()
    .then(function (payload) {
      utils.writeJson(res, payload, 200);
    })
    .catch(function (err) {
      utils.writeJson(res, err.message, 400);
    });
};
