'use strict';

/**
 * API controller for queue handling stuff.
 * @module controllers/queue
 * @license MIT
 * @author Kai KRETSCHMANN <kai@kretschmann.consulting>
 */

const utils = require('../utils/writer.js');
const eventEmitter = require('../utils/eventer').em;
const QueueService = require('../service/QueueService');

const log4js = require('log4js');
const logger = log4js.getLogger();
logger.level = process.env.LOGLEVEL || /* istanbul ignore next */ 'warn'; // LCOV_EXCL_LINE
const EVENTNAME = 'apihit';

/**
 * List all queues and entry counts.
 * @function listQueues
 * @public
 */
module.exports.listQueues = function listQueues (req, res, _next) {
  eventEmitter.emit(EVENTNAME, req);

  QueueService.listQueues()
    .then(function (payload) {
      utils.writeJson(res, payload, 200);
    })
    .catch(function (payload) {
      utils.writeJson(res, payload, 400);
    });
};
