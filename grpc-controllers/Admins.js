'use strict';

/**
 * @module grpc-controller
 * gRPC controller for admin methods mapping.
 * @license MIT
 * @author Kai KRETSCHMANN <kai@kretschmann.consulting>
 */

const eventEmitter = require('../utils/eventer').em;
const PackagesService = require('../service/PackagesService');

const log4js = require('log4js');
const logger = log4js.getLogger();
logger.level = process.env.LOGLEVEL || /* istanbul ignore next */ 'warn';
const EVENTNAME = 'grpchit';

/**
 * @method
 * Get count per creator.
 * @public
 **/
module.exports.serviceCountPackage = function serviceCountPackage (call, callback) {
  eventEmitter.emit(EVENTNAME, call);
  logger.info('gRPC countPackage');

  PackagesService.countPackage()
    .then(function (payload) {
      callback(null, payload);
    })
    .catch(function (payload) {
      callback(null, -1);
    });
};
