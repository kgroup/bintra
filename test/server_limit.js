// During the test the env variable is set to test
process.env.NODE_ENV = 'test';

const DomainModel = require('../models/domain.js');
const LoginModel = require('../models/login.js');
const UsersService = require('../service/UsersService.js');
const uauth = require('../utils/auth.js');

// Require the dev-dependencies
const appWait = require('../utils/appwait').appWait;
const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = require('chai').expect;
const should = chai.should(); // keep that line
const server = require('../app').app;
const mongoose = require('../app').mongoose;
const request = require('supertest');

const captureLogs = require('../testutils/capture-logs');

const log4js = require('log4js');
const logger = log4js.getLogger();
logger.level = process.env.LOGLEVEL || 'warn';

chai.use(chaiHttp);

let tokenUser = '';
let tokenShortUser = '';

function sleep (ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}

before(function (done) {
  appWait(done, __filename);
});

describe('server', () => {
  captureLogs();

  before(async () => {
    logger.info('run before');
    const adminUtil = mongoose.connection.db.admin();
    await adminUtil.ping();

    await DomainModel.deleteMany({});

    await LoginModel.deleteMany({
      name: 'max'
    });
    const oUserDefault = {
      username: 'max',
      email: 'test@example.com',
      password: 'xxx'
    };
    await UsersService.createUser(oUserDefault);
    await LoginModel.updateMany({
      name: 'max'
    }, {
      $set: {
        role: 'admin',
        status: 'active'
      }
    });

    await LoginModel.deleteMany({
      name: 'bob'
    });
    const oUserDummy = {
      username: 'bob',
      email: 'trash@example.com',
      password: 'abc'
    };
    await UsersService.createUser(oUserDummy);
    await LoginModel.updateMany({
      name: 'bob'
    }, {
      $set: {
        role: 'user',
        status: 'active'
      }
    });

    logger.info('Login to get token');
    tokenUser = uauth.issueToken('bob', 'user');
    logger.info('Token: ' + tokenUser);
    tokenShortUser = uauth.issueShortToken('bob', 'user');
    logger.info('Short living Token: ' + tokenShortUser);
    await sleep(1000);
  });

    describe('[BINTRA-54] Check limit headers', () => {
        it('[STEP-1] request normal', (done) => {
            request(server)
                .get('/v1/count')
                .expect(200)
                .then(res => {
					res.should.have.status(200);
                    done();
                })
                .catch(err => done(err));
        });
        it('[STEP-2] request more than lowlimit times', (done) => {
            request(server)
                .get('/v1/count')
                .expect(200)
                .then(res => {
					res.should.have.status(200);
                    done();
                })
                .catch(err => done(err));
        });
        it('[STEP-3] request more than lowlimit times', (done) => {
            request(server)
                .get('/v1/count')
                .expect(200)
                .then(res => {
					res.should.have.status(200);
                    done();
                })
                .catch(err => done(err));
        });
        it('[STEP-4] request more than lowlimit times', (done) => {
            request(server)
                .get('/v1/count')
                .expect(200)
                .then(res => {
					res.should.have.status(200);
                    done();
                })
                .catch(err => done(err));
        });
        it('[STEP-5] request more than lowlimit times', (done) => {
            request(server)
                .get('/v1/count')
                .expect(429)
                .then(res => {
					res.should.have.status(429);
                    done();
                })
                .catch(err => done(err));
        });
    });

  after(async () => {
    logger.info('after run');
    await LoginModel.deleteMany({
      name: 'max'
    });
  });
});
