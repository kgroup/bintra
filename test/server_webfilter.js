// During the test the env variable is set to test
process.env.NODE_ENV = 'test';

// Require the dev-dependencies
const appWait = require('../utils/appwait').appWait;
const chai = require('chai');
const expect = chai.expect;
const mocha = require('mocha')
const describe = mocha.describe
const should = chai.should(); // eslint-disable-line no-unused-vars
const it = mocha.it
const before = mocha.before;
const chaiHttp = require('chai-http');
const server = require('../app').app;
const request = require('supertest');
const httpMocks = require('node-mocks-http');
const pfilter = require('../controllers/pfilter');

const captureLogs = require('../testutils/capture-logs');

const log4js = require('log4js');
const logger = log4js.getLogger();
logger.level = process.env.LOGLEVEL || 'warn';

const sWrong = 'Invalid#(chars';
const sValid = 'Invalidchars';

chai.use(chaiHttp);

before(function (done) {
  appWait(done, __filename);
});

describe('webfilter', () => {
  captureLogs();

  describe('[BINTRA-29] GET good one', () => {
    it('[STEP-1] should redirect', (done) => {
      request(server)
        .get('/')
        .end((err, res) => {
          if (err) {
            done(err);
          }
          res.should.have.status(301);
          done();
        });
    });
  });

  describe('[BINTRA-30] Get bad urls', () => {
    it('[STEP-1] get double slashes', (done) => {
      request(server)
        .get('//v1/test')
        .end((err, res) => {
          if (err) {
            done(err);
          }
          res.should.have.status(400);
          done();
        });
    });
    it('[STEP-2] get dot env from root', (done) => {
      request(server)
        .get('/.env')
        .end((err, res) => {
          if (err) {
            done(err);
          }
          res.should.have.status(400);
          done();
        });
    });
    it('[STEP-3] get dot env from subfolder', (done) => {
      request(server)
        .get('/utils/.env')
        .end((err, res) => {
          if (err) {
            done(err);
          }
          res.should.have.status(400);
          done();
        });
    });
    it('[STEP-4] get some php script', (done) => {
      request(server)
        .get('/file.php')
        .end((err, res) => {
          if (err) {
            done(err);
          }
          res.should.have.status(400);
          done();
        });
    });
    it('[STEP-5] get some .git folder', (done) => {
      request(server)
        .get('/.git/config')
        .end((err, res) => {
          if (err) {
            done(err);
          }
          res.should.have.status(400);
          done();
        });
    });
    it('[STEP-6] get wp-admin folder', (done) => {
      request(server)
        .get('/wp-admin/admin-ajax')
        .end((err, res) => {
          if (err) {
            done(err);
          }
          res.should.have.status(400);
          done();
        });
    });
    it('[STEP-7] get count with .git arg', (done) => {
      request(server)
        .get('/v1/count?packageName=.git')
        .end((err, res) => {
          if (err) {
            done(err);
          }
          res.should.have.status(400);
          done();
        });
    });
  });

  describe('[BINTRA-53] direct api call for pfilter', () => {
    it('[STEP-1] Call non api path', (done) => {
      let bNextCalled = false;
      const request = httpMocks.createRequest({
          method: 'GET',
          url: '/wrong',
          openapi: {
			      schema: {
				      'x-security-scopes': 'abc'
			      }
		  }
      });
      const response = httpMocks.createResponse();
      pfilter(request, response, () => {bNextCalled = true} );
      expect(bNextCalled).to.be.true;
      done();
    });
    it('[STEP-2] Filter wrong name', (done) => {
      let bNextCalled = false;
      const request = httpMocks.createRequest({
          method: 'GET',
          url: '/v1/somemethod',
          params: {
            name: sWrong
          },
          query: {
            name: sWrong
          },
          openapi: {
			      schema: {
				      'x-security-scopes': 'abc'
			      }
		  }
      });
      const response = httpMocks.createResponse();
      logger.debug(`Param before ${request.params.name}`);
      pfilter(request, response, () => {bNextCalled = true} );
      const sTmp = request.query['name'];
      logger.debug(`Param after ${sTmp}`);
      expect(bNextCalled).to.be.true;
      expect(sTmp).to.not.equal(sWrong);
      expect(sTmp).to.equal(sValid);
      done();
    });
    it('[STEP-3] Filter wrong string', (done) => {
      let bNextCalled = false;
      const request = httpMocks.createRequest({
          method: 'GET',
          url: '/v1/somemethod',
          params: {
            packageName: sWrong
          },
          query: {
            packageName: sWrong
          },
          openapi: {
			      schema: {
				      'x-security-scopes': 'abc'
			      }
		  }
      });
      const response = httpMocks.createResponse();
      pfilter(request, response, () => {bNextCalled = true} );
      const sTmp = request.query['packageName'];
      expect(bNextCalled).to.be.true;
      expect(sTmp).to.not.equal(sWrong);
      expect(sTmp).to.equal(sValid);
      done();
    });
  });
});
