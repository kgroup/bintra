/**
 * Test API
 * @see DDATA-functional-API-numbers
 */

const appWait = require('../utils/appwait').appWait;
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../app').app;
const mongoose = require('../app').mongoose;
const should = chai.should();
const request = require('supertest');

const util = require('util');
const uauth = require('../utils/auth.js');

const captureLogs = require('../testutils/capture-logs');

const PackageModel = require('../models/package.js');
const LoginModel = require('../models/login.js');
const UsersService = require('../service/UsersService.js');

const log4js = require('log4js');
const logger = log4js.getLogger();
logger.level = process.env.LOGLEVEL || 'warn';

chai.use(chaiHttp);

let packageid = '';
let tokenUser;

before(function (done) {
  appWait(done, __filename);
});

describe('PFilter admin package server tests', function () {
  captureLogs();

  before(async () => {
    logger.info('run before');
    const adminUtil = mongoose.connection.db.admin();
    await adminUtil.ping();

    await PackageModel.deleteMany({});
    await LoginModel.deleteMany({});

    const oUserDefault = {
      username: 'max',
      email: 'test@example.com',
      password: 'xxx'
    };
    await UsersService.createUser(oUserDefault);
    await LoginModel.updateMany({
      name: 'max'
    }, {
      $set: {
        role: 'admin',
        status: 'active'
      }
    });
    tokenUser = uauth.issueToken('max', 'user');
    logger.info('Token: ' + tokenUser);

    const tsnow = new Date();
    const packageNew = new PackageModel({
      name: 'theName',
      version: 'theVersion',
      arch: 'theArchitecture',
      family: 'theFamily',
      hash: 'theHash',
      tscreated: tsnow,
      tsupdated: tsnow
    });
    await packageNew.save();
  });

  context('[BINTRA-52] search for package', () => {
    it('[STEP-1] get some packages listed with count', (done) => {
      request(server)
        .get('/v1/packagesfull')
        .auth(tokenUser, {
            type: 'bearer'
        })
        .query({
            count: 10
        })
        .end((err, res) => {
          if (err) {
            done(err);
          }
          res.should.have.status(200);
          packageid = res.body[0]._id;
          logger.info(packageid);
          done();
        });
    });
    it('[STEP-2] get some packages listed without count', (done) => {
        request(server)
            .get('/v1/packagesfull')
            .auth(tokenUser, {
                type: 'bearer'
            })
            .end((err, res) => {
            if (err) {
                done(err);
            }
            res.should.have.status(200);
            packageid = res.body[0]._id;
            logger.info(packageid);
            done();
          });
      });
    });

  after(async () => {
    logger.info('after run');
    await PackageModel.deleteMany({});
    await LoginModel.deleteMany({});
  });
});
