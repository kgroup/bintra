/**
 * Test API
 * @see DDATA-functional-API-numbers
 */

const appWait = require('../utils/appwait').appWait;
const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = chai.expect;
const chaiAsPromised = require('chai-as-promised');
const httpMocks = require('node-mocks-http');

const log4js = require('log4js');
const logger = log4js.getLogger();
logger.level = process.env.LOGLEVEL || 'info';

const captureLogs = require('../testutils/capture-logs');

chai.use(chaiAsPromised);
chai.use(chaiHttp);
chai.use(require('chai-json-schema'));

const FeedsController = require('../controllers/Feeds.js');

before(function (done) {
  appWait(done, __filename);
});

describe('Feed stuff', function () {
  captureLogs();

  before(async () => {
    logger.info('run before ' + __filename.slice(__dirname.length + 1));

	await captureLogs.getDatabaseConnection();
  });

  context('[BINTRA-48] feeds', function () {
    it('[STEP-1] direct feed api call with wrong type', (done) => {
        const request = httpMocks.createRequest({
            method: 'GET'
        });
        const response = httpMocks.createResponse();
        FeedsController.bintraFeed(request, response, 2, 'xxx');
        const rc = response._getStatusCode();
        expect(rc).equals(500);
        done();
    });
    it('[STEP-2] direct feed api call with RSS type', (done) => {
        const request = httpMocks.createRequest({
            method: 'GET'
        });
        const response = httpMocks.createResponse();
        FeedsController.bintraFeed(request, response, 2, 'rss');
        const rc = response._getStatusCode();
        expect(rc).equals(200);
        done();
    });
    it('[STEP-3] direct feed api call with ATOM type', (done) => {
        const request = httpMocks.createRequest({
            method: 'GET'
        });
        const response = httpMocks.createResponse();
        FeedsController.bintraFeed(request, response, 2, 'atom');
        const rc = response._getStatusCode();
        expect(rc).equals(200);
        done();
    });
    it('[STEP-4] direct feed api call with JSON type', (done) => {
        const request = httpMocks.createRequest({
            method: 'GET'
        });
        const response = httpMocks.createResponse();
        FeedsController.bintraFeed(request, response, 2, 'json');
        const rc = response._getStatusCode();
        expect(rc).equals(200);
        done();
    });
  });

  after(async () => {
    logger.info('after run');
  });
});
