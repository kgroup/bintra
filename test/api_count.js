/**
 * Test API
 * @see DDATA-functional-API-numbers
 */

const chai = require('chai');
const expect = chai.expect;
const chaiAsPromised = require('chai-as-promised');

const captureLogs = require('../testutils/capture-logs');

chai.use(chaiAsPromised);
chai.use(require('chai-json-schema'));

const PackagesService = require('../service/PackagesService.js');

const log4js = require('log4js');
const logger = log4js.getLogger();
logger.level = process.env.LOGLEVEL || 'warn';

describe('getCount', function () {
  captureLogs();

  before(async () => {
    logger.info('run before ' + __filename.slice(__dirname.length + 1));

	await captureLogs.getDatabaseConnection();
  });

  context('[BINTRA-1] get count', function () {
    it('[STEP-1] should generate number property', async () => {
      const countSchema = {
        title: 'count schema',
        type: 'object',
        required: ['count'],
        properties: {
          count: {
            type: 'number',
            minimum: 0
          }
        }
      };
      const result = await PackagesService.countPackage();
      return expect(result).to.be.jsonSchema(countSchema);
    });
  })
});
