// During the test the env variable is set to test
process.env.NODE_ENV = 'test';

// Require the dev-dependencies
const appWait = require('../utils/appwait').appWait;
const chai = require('chai');
const expect = chai.expect;
const should = chai.should();
const request = require('supertest');

const protoLoader = require('@grpc/proto-loader');
const grpc = require('@grpc/grpc-js');

const captureLogs = require('../testutils/capture-logs');

const log4js = require('log4js');
const logger = log4js.getLogger();
logger.level = process.env.LOGLEVEL || 'warn';

chai.use(require('chai-json-schema'));

let tokenUser = '';
let tokenShortUser = '';

function sleep (ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}

before(function (done) {
  appWait(done, __filename);
});

describe('gRPC server', () => {
  const gRPCuri = '127.0.0.1:' + process.env.GRPC_BIND_PORT;
  const gRPCproto = 'proto/route_guide.proto';
  var defn;
  var proto;
  var client;
  var empty = {};

  const countSchema = {
    title: 'count schema',
    type: 'object',
    required: ['count'],
    properties: {
      count: {
        type: 'number',
        minimum: 0
      }
    }
  };

  captureLogs();

  before(async () => {
    logger.info('run before');

    defn = protoLoader.loadSync(gRPCproto);
    proto = grpc.loadPackageDefinition(defn);
    logger.debug(`connect client to ${gRPCuri}`);
    client = new proto.routeguide.ApiService(gRPCuri, grpc.credentials.createInsecure());

    await sleep(1000);
  });

  describe('[BINTRA-34] getCount', () => {
    it('[STEP-1] call getCount', (done) => {
      client.getCount(empty, function(error, result) {
        if(error) {
          logger.error(error);
          done(error);
        } else {
          logger.info(result);
          expect(result).to.be.jsonSchema(countSchema);
          done();
        }
      });
    });
  });

  describe('[BINTRA-50] Check empty grpc config', () => {
    let envCache;

    // mocking an environment
    before(() => {
      envCache = process.env;
    });
    it('[STEP-1] request normal', (done) => {
      process.env.GRPC_BIND_PORT = undefined;
      client.getCount(empty, function(error, result) {
        if(error) {
          logger.error(error);
          done(error);
        } else {
          logger.info(result);
          expect(result).to.be.jsonSchema(countSchema);
          done();
        }
      });
    });
    // Trying to restore everything back
    after(() => {
      Object.keys(process.env).forEach((key) => { delete process.env[key] })
      Object.entries(envCache).forEach(([ key, value ]) => {
        if (key !== undefined) {
          process.env[key] = value;
        } // if
      })
    });
  });

  after(async () => {
    logger.info('after run');
  });
});
