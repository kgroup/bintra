// During the test the env variable is set to test
process.env.NODE_ENV = 'test';

const PackageModel = require('../models/package.js');
const LoginModel = require('../models/login.js');
const DomainModel = require('../models/domain.js');
const UsersService = require('../service/UsersService.js');
const util = require('util');
const uauth = require('../utils/auth.js');

// Require the dev-dependencies
const appWait = require('../utils/appwait').appWait;
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../app').app;
const mongoose = require('../app').mongoose;
const should = chai.should();
const request = require('supertest');

const log4js = require('log4js');
const logger = log4js.getLogger();
logger.level = process.env.LOGLEVEL || 'warn';

let tokenUser;

const captureLogs = require('../testutils/capture-logs');

chai.use(chaiHttp);

/**
 * Helper functions
 */
function getUserObject (username) {
  return new Promise(function (resolve, reject) {
    LoginModel.find({
      name: username
    })
      .then(itemFound => {
        if (itemFound.length === 1) {
          logger.info('Found user');
          resolve(itemFound[0]);
        } else {
          reject(new Error('Not found'));
        }
      })
      .catch(err => {
        logger.error('getUser failed: ' + err);
        reject(err);
      });
  });
}

before(function (done) {
  appWait(done, __filename);
});

describe('Domain stuff', () => {
  captureLogs();

  describe('[BINTRA-38] Check domain actions', () => {
    before(async () => {
      const adminUtil = mongoose.connection.db.admin();
      await adminUtil.ping();

      await DomainModel.deleteMany({});
      await UsersService.addDomain('demo.xyz');
      await LoginModel.deleteMany({
        name: 'max'
      });
  
      const oUserDefault = {
        username: 'max',
        email: 'test@example.com',
        password: 'xxx'
      };
      await UsersService.createUser(oUserDefault);
      await LoginModel.updateMany({
        name: 'max'
      }, {
        $set: {
          role: 'admin',
          status: 'active'
        }
      });
      logger.info('Login to get token');
      tokenUser = uauth.issueToken('max', 'user');
      logger.info('Token: ' + tokenUser);
      });

    it('[STEP-1] check get all domains', (done) => {
      request(server)
        .get('/v1/domains')
        .auth(tokenUser, {
          type: 'bearer'
        })
        .end((err, res) => {
          if (err) {
            logger.error(err);
            done(err);
          }
          logger.info('did get reply');
          res.should.have.status(200);
          res.should.be.json; // eslint-disable-line no-unused-expressions
          done();
        });
    });
    it('[STEP-2] check non existing domains', (done) => {
        request(server)
          .get('/v1/domain/nonexits')
          .auth(tokenUser, {
            type: 'bearer'
          })
          .end((err, res) => {
            if (err) {
              logger.error(err);
              done(err);
            }
            res.should.have.status(404);
            done();
          });
      });
    
  });

  after(async () => {
    logger.info('after run');
    await DomainModel.deleteMany({});
  });
});
