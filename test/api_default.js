/**
 * Test API
 * @see DDATA-functional-API-numbers
 */

const chai = require('chai');
const expect = chai.expect;
const chaiAsPromised = require('chai-as-promised');

const captureLogs = require('../testutils/capture-logs');

chai.use(chaiAsPromised);
chai.use(require('chai-json-schema'));

const PackageModel = require('../models/package.js');
const LoginModel = require('../models/login.js');

const PackagesService = require('../service/PackagesService.js');

const log4js = require('log4js');
const logger = log4js.getLogger();
logger.level = process.env.LOGLEVEL || 'warn';

describe('get default role funtions', function () {
  captureLogs();

  before(async () => {
    logger.info('run before ' + __filename.slice(__dirname.length + 1));

	await captureLogs.getDatabaseConnection();
    await PackageModel.deleteMany({});
  });

  context('[BINTRA-16] get packages from empty db', function () {
    it('[STEP-1] should have empty reply', async () => {
      const result = await PackagesService.listPackagesFull();
      return expect(result).to.have.length(0);
    });
  });

  context('[BINTRA-39] get health status', function () {
    it('[STEP-1] should have OK reply', async () => {
      const result = await PackagesService.healthCheck();
      expect(result).to.be.a('boolean');
      return expect(result).to.be.true;
    });
  });

  context('[BINTRA-17] get packages from filled db', function () {
    let tsnow;
    before(async () => {
      tsnow = new Date();
      const packageNew = new PackageModel({
        name: 'theName',
        version: 'theVersion',
        arch: 'theArchitecture',
        family: 'theFamily',
        hash: 'theHash',
        tscreated: tsnow,
        tsupdated: tsnow
      });
      await packageNew.save();
    });
    it('[STEP-1] should have one reply', async () => {
      const result = await PackagesService.listPackagesFull();
      return expect(result).to.have.length(1);
    });
    it('[STEP-2] get package by values', async () => {
      const result = await PackagesService.listPackage('theName', 'theVersion', 'theArchitecture', 'theFamily');
      return expect(result).to.have.length(1);
    });
    it('[STEP-3] search packages by name', async () => {
      const result = await PackagesService.searchPackages({
        packageName: 'theName'
      });
      return expect(result).to.have.length(1);
    });
    it('[STEP-4] search packages by version', async () => {
      const result = await PackagesService.searchPackages({
        packageVersion: 'theVersion'
      });
      return expect(result).to.have.length(1);
    });
    it('[STEP-5] search packages by arch', async () => {
      const result = await PackagesService.searchPackages({
        packageArch: 'theArchitecture'
      });
      return expect(result).to.have.length(1);
    });
    it('[STEP-6] search packages by family', async () => {
      const result = await PackagesService.searchPackages({
        packageFamily: 'theFamily'
      });
      return expect(result).to.have.length(1);
    });
    it('[STEP-7] search packages by hash', async () => {
      const result = await PackagesService.searchPackages({
        packageHash: 'theHash'
      });
      return expect(result).to.have.length(1);
    });
    it('[STEP-8] search packages by count', async () => {
      const result = await PackagesService.searchPackages({
        count: 1
      });
      return expect(result).to.have.length(1);
    });
    it('[STEP-9] search packages by tscreated', async () => {
      const result = await PackagesService.searchPackages({
        tscreated: tsnow
      });
      return expect(result).to.have.length(1);
    });
    it('[STEP-10] search packages by tsupdated', async () => {
      const result = await PackagesService.searchPackages({
        tsupdated: tsnow
      });
      return expect(result).to.have.length(1);
    });
    it('[STEP-11] search packages by wildcard name', async () => {
      const result = await PackagesService.searchPackages({
        packageName: 'the*'
      });
      return expect(result).to.have.length(1);
    });
    it('[STEP-12] search packages by name and version', async () => {
      const result = await PackagesService.searchPackages({
        packageName: 'theName',
        packageVersion: 'theVersion'
      });
      return expect(result).to.have.length(1);
    });
    it('[STEP-13] list single non existing package', (done) => {
      PackagesService.listPackageSingle('00112233445566778899aabb')
        .then(itemFound => {
          logger.error('Should not pass');
          done(new Error('Should not pass'));
        })
        .catch(err => {
          logger.info('expected error');
          expect(err).to.exist;
          done();
        });
    });
    it('[STEP-14] find package by direct api call', (done) => {
      logger.info('create promise object');
      let p = new Promise(function (resolve, reject) {
        PackagesService.findPackage(resolve, reject, 'theName', 'theVersion', 'theArchitecture', 'theFamily');
      });
      logger.info('call promise object');
      p
        .then(rc => {
          logger.info('did work');
          expect(rc).to.have.length(1);
          done();
        })
        .catch(err => {
          logger.error('did not pass');
          expect(err).to.exist;
          done(err);
        })
    });
    it('[STEP-15] miss package by direct api call', (done) => {
      logger.info('create promise object');
      let p = new Promise(function (resolve, reject) {
        PackagesService.findPackage(resolve, reject, 'NottheName', 'NottheVersion', 'NottheArchitecture', 'NottheFamily');
      });
      logger.info('call promise object');
      p
        .then(rc => {
          logger.info('did somehow work with no single match');
          expect(rc).to.have.length(0);
          done(new Error('Should not find'));
        })
        .catch(err => {
          logger.error('did trigger');
          expect(err).to.exist;
          done();
        })
    });
  });

  after(async () => {
    logger.info('after run');
    await PackageModel.deleteMany({});
  });
});
