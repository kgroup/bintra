/**
 * Test API
 * @see DDATA-functional-API-numbers
 */

const appWait = require('../utils/appwait').appWait;
const chai = require('chai');
const should = chai.should();
const expect = chai.expect;
const chaiAsPromised = require('chai-as-promised');
const request = require('supertest');

const captureLogs = require('../testutils/capture-logs');
const getFakeId = require('../testutils/fakeid');

chai.use(chaiAsPromised);
chai.use(require('chai-json-schema'));

const PackageModel = require('../models/package.js');
const LoginModel = require('../models/login.js');
const DomainModel = require('../models/domain.js');

const UsersService = require('../service/UsersService.js');
const PackagesService = require('../service/PackagesService.js');

const log4js = require('log4js');
const logger = log4js.getLogger();
logger.level = process.env.LOGLEVEL || 'warn';

let JWT;
let idUser;

describe('User stuff', function () {
  captureLogs();

  before(async () => {
    logger.info('run before ' + __filename.slice(__dirname.length + 1));

	  await captureLogs.getDatabaseConnection();

    tsnow = new Date();
    await PackageModel.deleteMany({});
    await LoginModel.deleteMany({});
    await DomainModel.deleteMany({});

    const domainNew = new DomainModel({
      name: 'theDomain',
      tscreated: tsnow
    });
    await domainNew.save();

    await UsersService.addDomain('demo.xyz');

    const u = {
      username: 'max',
      email: 'test@example.com',
      password: 'xxx'
    };
    await UsersService.createUser(u);
    await LoginModel.updateMany({}, {
      $set: {
        status: 'active'
      }
    });
  });

  context('[BINTRA-20] login', function () {
    it('[STEP-1] Check user was created', (done) => {
      LoginModel.find({})
        .then(itemFound => {
          logger.info('Query logins worked, mount=' + itemFound.length);
          itemFound.should.have.length(1);
          done();
        })
        .catch(err => done(err));
    });
    it('[STEP-2] List users', (done) => {
      UsersService.listUsers()
        .then(itemFound => {
          itemFound.should.have.length(1);
          idUser = itemFound[0]._id;
          done();
        })
        .catch(err => done(err));
    });
    it('[STEP-3] List user', (done) => {
      UsersService.listUser(idUser)
        .then(itemFound => {
          itemFound.should.have.property('email');
          done();
        })
        .catch(err => done(err));
    });
    it('[STEP-4] Check role', (done) => {
      UsersService.hasRole('max', ['user'])
        .then(itemFound => {
	      expect(itemFound).to.be.an('boolean');
          done();
        })
        .catch(err => done(err));
    });
    it('[STEP-5] Check role with wrong role', (done) => {
      UsersService.hasRole('max', ['admin'])
        .then(itemFound => {
          logger.error('Should not pass');
          done(new Error('Should not pass'));
        })
        .catch(err => {
          logger.info('expected error');
          expect(err).to.exist;
          done();
        });
    });
    it('[STEP-6] Check role with wrong user', (done) => {
      UsersService.hasRole('sam', ['user'])
        .then(itemFound => {
          logger.error('should not pass');
          done(new Error('Should not pass'));
        })
        .catch(err => {
          logger.info('expected error');
          expect(err).to.exist;
          done();
        });
    });
    it('[STEP-7] Check active user', (done) => {
      UsersService.isActiveUser('max')
        .then(itemFound => {
	      expect(itemFound).to.be.an('boolean');
          done();
        })
        .catch(err => done(err));
    });
    it('[STEP-8] Check active with wrong user', (done) => {
      UsersService.isActiveUser('sam')
        .then(itemFound => {
          logger.error('should not pass');
          done(new Error('Should not pass'));
        })
        .catch(err => {
          logger.info('expected error');
          expect(err).to.exist;
          done();
        });
    });
    it('[STEP-9] List wrong user', (done) => {
      const fakeId = getFakeId(idUser);
      UsersService.listUser(fakeId)
        .then(itemFound => {
          logger.error('should not pass');
          done(new Error('Should not pass'));
        })
        .catch(err => {
          logger.info('expected error');
          expect(err).to.exist;
          done();
        });
    });
    it('[STEP-10] Check role function good', (done) => {
      logger.info('create promise object');
      let p = new Promise(function (resolve, reject) {
        UsersService.roleCheck('sam1', [{role: 'user1'}], ['user1'], resolve, reject);
      });
      logger.info('call promise object');
      p
        .then(rc => {
          logger.info('did work');
          expect(rc).to.be.an('boolean');
          done();
        })
        .catch(err => {
          logger.error('did not pass');
          expect(err).to.exist;
          done(err);
        })
    });
    it('[STEP-11] Check role function bad', (done) => {
      logger.info('create promise object');
      let p = new Promise(function (resolve, reject) {
        UsersService.roleCheck('sam2', [{role: 'user2'}], ['xuser2'], resolve, reject);
      });
      logger.info('call promise object');
      p
        .then(_rc => {
          logger.error('did not pass');
          done(new Error('Wrong OK'));
        })
        .catch(err => {
          logger.info('did work');
          expect(err).to.exist;
          done();
        })
    });
    it('[STEP-12] Check role function empty array', (done) => {
      logger.info('create promise object');
      let p = new Promise(function (resolve, reject) {
        UsersService.roleCheck('sam3', [], ['xuser3'], resolve, reject);
      });
      logger.info('call promise object');
      p
        .then(_rc => {
          logger.error('did not pass');
          done(new Error('Wrong OK'));
        })
        .catch(err => {
          logger.info('did work');
          expect(err).to.exist;
          done();
        })
    });
  });

  context('[BINTRA-47] simple action without LOGLEVEL set', function () {
    let envCache;

    before(() => {
      envCache = process.env;
      delete process.env.LOGLEVEL;
    });
    it('[STEP-1] List users', (done) => {
      UsersService.listUsers()
        .then(itemFound => {
          itemFound.should.have.length(1);
          idUser = itemFound[0]._id;
          done();
        })
        .catch(err => done(err));
    });
    // Trying to restore everything back
    after(() => {
      process.env = envCache;
    });
  });
  
  context('[BINTRA-42] domain user blacklist', function () {
    it('[STEP-1] Create blocked user', (done) => {
      const u = {
        username: 'blocky',
        email: 'test@demo.xyz',
        password: 'xxx'
      };
      UsersService.createUser(u)
      .then(itemFound => {
        logger.error('should not pass');
        done(new Error('Should not pass'));
      })
      .catch(err => {
        logger.info('expected error');
        expect(err).to.exist;
        done();
      });
    })
  });

  context('[BINTRA-21] add package as user', () => {
    it('[STEP-1] Add package in users name', (done) => {
      PackagesService.validatePackage('theName', 'theVersion', 'theArch', 'debian', '10.2', 'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855', 'max')
        .then(itemFound => {
          itemFound.should.have.length(1);
          done();
        })
        .catch(err => done(err));
    });
    it('[STEP-2] Add again package in users name', (done) => {
      PackagesService.validatePackage('theName', 'theVersion', 'theArch', 'debian', '10.2', 'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855', 'max')
        .then(itemFound => {
          itemFound.should.have.length(1);
          done();
        })
        .catch(err => done(err));
    });
    it('[STEP-3] Add again package in wrong users name', (done) => {
      PackagesService.validatePackage('theName', 'theVersion', 'theArch', 'debian', '10.2', 'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855', 'maria')
        .then(itemFound => {
          logger.error(`should not pass ${itemFound}`);
          done(new Error('Should not pass'));
        })
        .catch(err => {
          logger.info('expected error');
          expect(err).to.exist;
          done();
        });
    });
  });

  context('[BINTRA-22] Destroy user', () => {
    let idUser;
    it('[STEP-1] List users once more', (done) => {
      UsersService.listUsers()
        .then(itemFound => {
          itemFound.should.have.length(1);
          idUser = itemFound[0]._id;
          done();
        })
        .catch(err => done(err));
    });
    it('[STEP-2] Delete user', (done) => {
      UsersService.deleteUser(idUser)
        .then(itemFound => {
          itemFound.should.have.property('status', 'deleted');
          done();
        })
        .catch(err => done(err));
    });
    it('[STEP-3] Delete wrong user', (done) => {
      const fakeId = getFakeId(idUser);
      UsersService.deleteUser(fakeId)
        .then(itemFound => {
          logger.error('should not pass');
          done(new Error('Should not pass'));
        })
        .catch(err => {
          logger.info('expected error');
          expect(err).to.exist;
          done();
        });
    });
  });

  after(async () => {
    logger.info('after run');
    await PackageModel.deleteMany({});
    await LoginModel.deleteMany({});
  });
});
