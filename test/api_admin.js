/**
 * Test API for admin level backend lib calls
 * @see DDATA-functional-API-numbers
 */

const chai = require('chai');
const expect = chai.expect;
const chaiAsPromised = require('chai-as-promised');

const captureLogs = require('../testutils/capture-logs');

chai.use(chaiAsPromised);
chai.use(require('chai-json-schema'));

const PackageModel = require('../models/package.js');
const LoginModel = require('../models/login.js');

const computeDBurl = require('../conf').computeDBurl;

const PackagesService = require('../service/PackagesService.js');
const UsersService = require('../service/UsersService.js');

const log4js = require('log4js');
const logger = log4js.getLogger();
logger.level = process.env.LOGLEVEL || 'warn';

describe('admin only functions', function () {
  captureLogs();

  before(async () => {
    logger.info('run before ' + __filename.slice(__dirname.length + 1));

	await captureLogs.getDatabaseConnection();
  });

  context('[BINTRA-14] delete package by id', function () {
    before(async () => {
      await PackageModel.deleteMany({});
      const tsnow = new Date();
      const packageNew = new PackageModel({
        name: 'theName',
        version: 'theVersion',
        arch: 'theArchitecture',
        family: 'theFamily',
        hash: 'theHash',
        tscreated: tsnow,
        tsupdated: tsnow
      });
      await packageNew.save();
    });
    it('[STEP-1] should have one reply', async () => {
      let result = await PackagesService.listPackagesFull();
      const theID = result[0]._id;
      logger.info('ID=' + theID);
      await PackagesService.deletePackageById(theID);
      result = await PackagesService.listPackagesFull();
      return expect(result).to.have.length(0);
    });
  });

  context('[BINTRA-15] get full package info', function () {
    before(async () => {
      await PackageModel.deleteMany({});
      const tsnow = new Date();
      const packageNew = new PackageModel({
        name: 'theName',
        version: 'theVersion',
        arch: 'theArchitecture',
        family: 'theFamily',
        hash: 'theHash',
        tscreated: tsnow,
        tsupdated: tsnow
      });
      await packageNew.save();
    });
    it('[STEP-1] packagesFull', async () => {
      const result = await PackagesService.listPackagesFull(111);
      return expect(result).to.have.length(1);
    });
  });

  context('[BINTRA-44] get logauth', function () {
    it('[STEP-1] Check logauth', (done) => {
      UsersService.getLogauth()
        .then(itemFound => {
          expect(itemFound).to.be.an('array');
          logger.info('Query logauth worked, length=' + itemFound.length);
          done();
        })
        .catch(err => done(err));
    });
  });

  context('[BINTRA-46] check computeDBurl conf paths', function () {
    let saveMongoUrl = '';
    let bMongoUrl = false;
    let saveMongoDb = '';
    let bMongoDb = false;
    let saveMongoReplica = '';
    let bMongoReplica = false;
    let saveMongoUsername = '';
    let bMongoUsername = false;
    let saveMongoPassword = '';
    let bMongoPassword = false;
    let saveMongoPort = '';
    let bMongoPort = false;
    let saveMongoHostname = '';
    let bMongoHostname = false;

    before(async () => {
      if (process.env.MONGO_URL) {
        logger.info('Saving MONGO_URL');
        saveMongoUrl = process.env.MONGO_URL;
        bMongoUrl = true;
      }
      if (process.env.MONGO_DB) {
        logger.info('Saving MONGO_DB');
        saveMongoDb = process.env.MONGO_DB;
        bMongoDb = true;
      }
      if (process.env.MONGO_REPLICA) {
        logger.info('Saving MONGO_REPLICA');
        saveMongoReplica = process.env.MONGO_REPLICA;
        bMongoReplica = true;
      }
      if (process.env.MONGO_USERNAME) {
        logger.info('Saving MONGO_USERNAME');
        saveMongoUsername = process.env.MONGO_USERNAME;
        bMongoUsername = true;
      }
      if (process.env.MONGO_PASSWORD) {
        logger.info('Saving MONGO_PASSWORD');
        saveMongoPassword = process.env.MONGO_PASSWORD;
        bMongoPassword = true;
      }
      if (process.env.MONGO_PORT) {
        logger.info('Saving MONGO_PORT');
        saveMongoPort = process.env.MONGO_PORT;
        bMongoPort = true;
      }
      if (process.env.MONGO_HOSTNAME) {
        logger.info('Saving MONGO_HOSTNAME');
        saveMongoPort = process.env.MONGO_HOSTNAME;
        bMongoHostname = true;
      }
    });

    it('[STEP-1] Check MONGO_URL', (done) => {
      process.env.MONGO_URL = 'mongodb://localhost/somedb';
      const mongoUrl = computeDBurl();
      logger.info(`Query ${mongoUrl}`);
      expect(mongoUrl).to.be.an('string');
      delete process.env.MONGO_URL;
      done();
    });
    it('[STEP-2] Check MONGO_REPLICA', (done) => {
      process.env.MONGO_DB = 'somedb';
      process.env.MONGO_REPLICA = 'localhost';
      const mongoUrl = computeDBurl();
      logger.info(`Query ${mongoUrl}`);
      expect(mongoUrl).to.be.an('string');
      delete process.env.MONGO_DB;
      delete process.env.MONGO_REPLICA;
      done();
    });
    it('[STEP-3] Check MONGO_HOST-PORT-DB', (done) => {
      process.env.MONGO_DB = 'somedb';
      process.env.MONGO_HOSTNAME = 'localhost';
      process.env.MONGO_PORT = 12345;
      const mongoUrl = computeDBurl();
      logger.info(`Query ${mongoUrl}`);
      expect(mongoUrl).to.be.an('string');
      delete process.env.MONGO_DB;
      delete process.env.MONGO_HOSTNAME;
      delete process.env.MONGO_PORT;
      done();
    });
    it('[STEP-4] Check MONGO_HOSTNAME-PORT-DBUSER', (done) => {
      process.env.MONGO_DB = 'somedb';
      process.env.MONGO_HOSTNAME = 'localhost';
      process.env.MONGO_PORT = 12345;
      process.env.MONGO_USERNAME = 'dbuser';
      process.env.MONGO_PASSWORD = 'dbpasswd';
      const mongoUrl = computeDBurl();
      logger.info(`Query ${mongoUrl}`);
      expect(mongoUrl).to.be.an('string');
      delete process.env.MONGO_DB;
      delete process.env.MONGO_HOSTNAME;
      delete process.env.MONGO_PORT;
      delete process.env.MONGO_USERNAME;
      delete process.env.MONGO_PASSWORD;
      done();
    });
    it('[STEP-5] Check wrong data', (done) => {
      const mongoUrl = computeDBurl();
      logger.info(`Query ${mongoUrl}`);
      expect(mongoUrl).to.be.empty; // eslint-disable-line no-unused-expressions
      done();
    });

    after(async () => {
      if (bMongoUrl) {
        process.env.MONGO_URL = saveMongoUrl;
      } else {
        delete process.env.MONGO_URL;
      }
      if (bMongoDb) {
        process.env.MONGO_DB = saveMongoDb;
      } else {
        delete process.env.MONGO_DB;
      }
      if (bMongoReplica) {
        process.env.MONGO_REPLICA = saveMongoReplica;
      } else {
        delete process.env.MONGO_REPLICA;
      }
      if (bMongoUsername) {
        process.env.MONGO_USERNAME = saveMongoUsername;
      } else {
        delete process.env.MONGO_USERNAME;
      }
      if (bMongoPassword) {
        process.env.MONGO_PASSWORD = saveMongoPassword;
      } else {
        delete process.env.MONGO_PASSWORD;
      }
      if (bMongoPort) {
        process.env.MONGO_PORT = saveMongoPort;
      } else {
        delete process.env.MONGO_PORT;
      }
      if (bMongoHostname) {
        process.env.MONGO_HOSTNAME = saveMongoHostname;
      } else {
        delete process.env.MONGO_HOSTNAME;
      }
    });
  });

  after(async () => {
    logger.info('after run');
    await PackageModel.deleteMany({});
  });
});
