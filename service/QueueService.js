'use strict';

/**
 * Plain user functionality in service methods.
 * @module service/queue
 * @license MIT
 * @author Kai KRETSCHMANN <kai@kretschmann.consulting>
 */

const Queue = require('node-resque').Queue;

const log4js = require('log4js');
const logger = log4js.getLogger();
logger.level = process.env.LOGLEVEL || /* istanbul ignore next */ 'warn'; // LCOV_EXCL_LINE

const qworker = require('../worker/main');

/**
 * List queues.
 * @method listQueues
 * @public
 * @returns {Promise} array of entries or error
 **/
exports.listQueues = function () {
  return new Promise(function (resolve, reject) {
    logger.debug('In list service');
    const q = new Queue({ connection: qworker.connectionDetails });
    q.on('error', function (error) {
      logger.error(error);
    });
    q.connect().then(_c => {
      q.queues()
        .then(async (item) => {
          const r = [];
          for (const value of item) {
            const l = await q.length(value);
            r.push({ id: value, count: l });
          }
          resolve(r);
        })
        .catch(err => {
          logger.error(`Not OK: ${err}`);
          reject(err);
        });
    });
  });
};
