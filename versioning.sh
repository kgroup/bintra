#!/bin/bash
#
# Update version by
# - run "npm version patch"
# -- will run this file in stage 'version' from package.json

VERSION=`node -p "require('./package.json').version"`
echo $VERSION

sed -i -E 's/(sonar.projectVersion=).*/\1'$VERSION'/' sonar-project.properties
sed -i -E 's/(\s+version: ).*/\1'$VERSION'/' api/swagger.yaml
sed -i -E 's/(.*:bintra:)[^:]+(:.*)/\1'$VERSION'\2/' VERSION.cpe
sed -i -E 's/(.*[:]bintra[: ])[^:]+([:"<].*)/\1'$VERSION'\2/' VERSION.xml
sed -i -E 's/(.*>bintra )[0-9.]+(<.*)/\1'$VERSION'\2/' VERSION.xml
