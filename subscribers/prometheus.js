/**
 * Event subscribe for prometheus statistics.
 * @module subscribers/prometheus
 * @license MIT
 * @author Kai KRETSCHMANN <kai@kretschmann.consulting>
 */

const eventEmitter = require('../utils/eventer').em;

const log4js = require('log4js');
const logger = log4js.getLogger();
logger.level = process.env.LOGLEVEL || /* istanbul ignore next */ 'warn';

/**
 * Subscribe to apihit event.
 * @function apihit
 * @public
 * @param {object} req The web request object
 */
eventEmitter.on('apihit', function getPrometheusApiHit (req) {
  logger.debug('In prometheus subscriber');

  if (req.appCounter == null) return;
  req.appCounter.inc();
});

module.exports = {}
