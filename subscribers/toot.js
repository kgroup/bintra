/**
 * Event subscribe for mastodon publish.
 * @module subscribers/toot
 * @license MIT
 * @author Kai KRETSCHMANN <kai@kretschmann.consulting>
 */
const eventEmitter = require('../utils/eventer').em;

const log4js = require('log4js');
const logger = log4js.getLogger();
logger.level = process.env.LOGLEVEL || /* istanbul ignore next */ 'warn';

const myworker = require('../worker/main');

/**
 * Subscribe to putdata event.
 * @function putdata
 * @public
 * @param {string} packageName package data part
 * @param {string} packageVersion package data part
 * @param {string} packageArch package data part
 * @param {string} packageFamily package data part
 * @param {string} packageHash package data part
 * @param {boolean} isnew package data part
 */
eventEmitter.on('putdata', function getPutDataHit (packageName, packageVersion, packageArch, packageFamily, packageHash, isnew) {
  if (process.env.TOOTAUTH === 'XXX') return;
  logger.debug('In toot subscriber');

  let t;
  if (isnew) {
    t = `Add new hash ${packageHash} for ${packageName} (${packageVersion}) for ${packageArch} #${packageFamily}`;
  } else {
    logger.info('Skip toot if not new');
    return;
  }

  t = t + ' #binarytransparency';

  myworker.doqueue(t);
});

module.exports = {}
