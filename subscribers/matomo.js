/**
 * Event subscribe for matomo statistics.
 * @module subscribers/matomo
 * @license MIT
 * @author Kai KRETSCHMANN <kai@kretschmann.consulting>
 */

const eventEmitter = require('../utils/eventer').em;

const log4js = require('log4js');
const logger = log4js.getLogger();
logger.level = process.env.LOGLEVEL || /* istanbul ignore next */ 'debug';

const MatomoTracker = require('matomo-tracker');
let matomo = null;
logger.debug(`MATOMO url ${process.env.MATOMO_URL} !`);
if ((process.env.MATOMO_URL !== undefined) && (process.env.MATOMO_URL !== null) && (process.env.MATOMO_URL !== '')) {
  matomo = new MatomoTracker(process.env.MATOMO_ID, process.env.MATOMO_URL);
  logger.info('MATOMO connected');
  matomo.on('error', function (err) {
    logger.error(`Matomo error: ${err}`);
  });
} else {
  logger.warn('No matomo url defined');
}
const baseUrl = 'https://api.binarytransparency.net';

/**
 * Get simple random number for Matomo requests.
 * @function randomInteger
 * @private
 * @param {int} min number from
 * @param {int} max number to
 * @returns {int} random number
 */
function randomInteger (min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

/**
 * Get remote IP number by connection or header data.
 * @function getRemoteAddr
 * @private
 * @param {object} req The web request object
 * @returns {string} IP number
 */
function getRemoteAddr (req) {
  if (req.headers['x-forwarded-for']) {
    const aIPs = req.headers['x-forwarded-for'].split(',');
    const firstIP = aIPs[0];
    logger.info(`Use x-forwarded-for first entry ${firstIP}`);
    return firstIP;
  }
  if (req.headers['x-real-ip']) {
    const realIP = req.headers['x-real-ip'];
    logger.info(`Use x-real-ip header ${realIP}`);
    return realIP;
  }

  if (req.ip) {
    let reqIp = req.ip;
    if (reqIp === '::ffff:127.0.0.1') reqIp = null; // fix for test scripts
    logger.info(`Use req.ip ${reqIp}`);
    return reqIp;
  }

  if (req._remoteAddress) {
    const reqRa = req._remoteAddress;
    logger.info(`Use req._remoteAddress ${reqRa}`);
    return reqRa;
  }

  const sock = req.socket;
  if (sock.socket) {
    const ssra = sock.socket.remoteAddress;
    logger.info(`Use sock.socket.remoteAddress ${ssra}`);
    return ssra;
  }

  const sra = sock.remoteAddress;
  logger.info(`Use sock.remoteAddress ${sra}`);
  return sra;
}

/**
 * Subscribe to apihit event.
 * @function apihit
 * @public
 * @param {object} req The web request object
 */
eventEmitter.on('apihit', function getApiHit (req) {
  logger.info('In subscriber apihit');

  // Verify some mandatory stuff
  if (matomo == null) return;
  const tokenauth = process.env.MATOMO_TOKEN_AUTH;
  if (typeof tokenauth === 'undefined') {
    logger.warn('token auth undefined');
    return;
  }

  // Setup values into variables
  const url = req.url;
  const urlparts = url.split('/');
  const urlMethod = urlparts[0] + '/' + urlparts[1] + '/' + urlparts[2];
  const reqMethod = req.method;
  const reqUseragent = req.headers['user-agent'];
  const reqLanguage = req.headers['accept-language'];
  const remoteIp = getRemoteAddr(req);
  const reqRand = randomInteger(100000, 999999); // fixed width, low entropy
  let reqUid;
  if (req.auth) { // if we have a user
    reqUid = req.auth.sub;
  }

  // construct object of values
  const oTrack = {
    url: baseUrl + urlMethod,
    action_name: 'API call',
    token_auth: tokenauth,
    apiv: 1,
    rand: reqRand,
    e_c: 'API',
    e_a: urlMethod,
    e_n: reqMethod,
    cvar: JSON.stringify({
      1: ['API version', urlparts[1]],
      2: ['HTTP method', reqMethod]
    })
  };
  if (reqUid) {
    oTrack.uid = reqUid;
  }
  if (remoteIp !== null) {
    oTrack.cip = remoteIp;
  }
  if (typeof reqUseragent !== 'undefined') {
    oTrack.ua = reqUseragent;
  }
  if (typeof reqLanguage !== 'undefined') {
    oTrack.lang = reqLanguage;
  }

  // trigger Matomo API
  logger.debug(oTrack);
  matomo.track(oTrack);
});

/**
 * Subscribe to grpchit event.
 * @function grpchit
 * @public
 * @param {object} call The grpc request object
 */
eventEmitter.on('grpchit', function getApiHit (call) {
  logger.debug('In subscriber GRPC');

  // Verify some mandatory stuff
  if (matomo == null) return;
  const tokenauth = process.env.MATOMO_TOKEN_AUTH;
  if (typeof tokenauth === 'undefined') {
    logger.warn('token auth undefined');
    return;
  }

  const url = call.call.handler.path;
  const urlparts = url.split('/');
  const urlMethod = urlparts[0] + '/' + urlparts[1] + '/' + urlparts[2];
  const peerIP = call.getPeer().split(':')[0];
  const reqUseragent = call.metadata.get('user-agent')[0];
  const reqRand = randomInteger(100000, 999999); // fixed width, low entropy
  logger.debug(`IP of peer: ${peerIP}`);

  // construct object of values
  const oTrack = {
    url: baseUrl + urlMethod,
    action_name: 'gRPC call',
    token_auth: tokenauth,
    cip: peerIP,
    e_c: 'gRPC',
    e_a: urlMethod,
    rand: reqRand,
    ua: reqUseragent
  };

  // trigger Matomo API
  logger.debug(oTrack);
  matomo.track(oTrack);
});

/**
 * Subscribe to posthit event.
 * @function posthit
 * @public
 * @param {string} urlpath The path request string
 */
eventEmitter.on('posthit', function getPostHit (urlpath) {
  logger.debug('In subscriber POST');

  // Verify some mandatory stuff
  if (matomo == null) return;
  const tokenauth = process.env.MATOMO_TOKEN_AUTH;
  if (typeof tokenauth === 'undefined') {
    logger.warn('token auth undefined');
    return;
  }

  const reqRand = randomInteger(100000, 999999); // fixed width, low entropy

  // construct object of values
  const oTrack = {
    url: baseUrl + urlpath,
    action_name: 'POST call',
    token_auth: tokenauth,
    rand: reqRand
  };

  // trigger Matomo API
  logger.debug(oTrack);
  matomo.track(oTrack);
});

function reportBadRequest (req) {
  if (matomo == null) return;

  const url = req.url;
  const reqUseragent = req.headers['user-agent'];
  const reqLanguage = req.headers['accept-language'];

  matomo.track({
    url: baseUrl + url,
    action_name: 'bad request',
    token_auth: process.env.MATOMO_TOKEN_AUTH,
    cip: getRemoteAddr(req),
    ca: 1,
    ua: reqUseragent,
    lang: reqLanguage
  });
};

/**
 * Subscribe to noapicall event.
 * @function noApiCall
 * @public
 * @param {string} req The request
 */
eventEmitter.on('noapicall', function noApiCall (req) {
  reportBadRequest(req);
});

/**
 * Subscribe to badrequest event.
 * @function badRequest
 * @public
 * @param {string} req The request
 */
eventEmitter.on('badrequest', function badRequest (req) {
  reportBadRequest(req);
});

module.exports = {}
