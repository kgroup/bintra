'use strict';

/**
 * Worker subfunction for toots.
 * @module worker/w_toot
 * @license MIT
 * @author Kai KRETSCHMANN <kai@kretschmann.consulting>
 */

const Masto = require('masto');
const log4js = require('log4js');
const logger = log4js.getLogger();
logger.level = process.env.LOGLEVEL || /* istanbul ignore next */ 'warn';

/**
 * Submit a toot to the world.
 * @function dotoot
 * @public
 * @param {object} t The mastodon toot object
 */
function dotoot (t) {
  const M = Masto.createRestAPIClient({
    accessToken: process.env.TOOTAUTH,
    url: process.env.TOOTAPI
  });

  logger.debug(`!!! Tooting ${t}`);
  try {
    M.v1.statuses.create({ status: t, visibility: 'public' }).then(resp => {
      logger.info('Did post status');
    });
  } catch (err) {
    logger.error(`Error tooting ${err}`);
  }
}

module.exports = dotoot;
