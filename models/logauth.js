// models/logauth.js

const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const LogauthSchema = new Schema({
  tssuccess: {
    type: Date,
    required: false
  },
  tsexpire: {
    type: Date,
    required: false
  },
  validity: {
    type: Number,
    required: true
  },
  remoteip: {
    type: String,
    required: true
  },
  countexpire: {
    type: Number,
    required: true,
    min: 0,
    default: 0
  },
  username: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'LoginModel'
  }
});

LogauthSchema.index({
  remoteip: 1,
  username: 1
}, {
  unique: true
});

module.exports = mongoose.model('LogauthModel', LogauthSchema);
