const globals = require('globals');

// eslint.config.js
module.exports = [
  {
	ignores: [
      'myoas/**',
      'static/**',
      'workflows/**',
      'mochawesome-report/**',
      'pub_mkdocs/**',
      'out/**',
      'documentation/**',
      'bintra-*/**',
      '**/test/**/*.js',
      'testutils/**'
	]
  },
  {
	languageOptions: {
      sourceType: "commonjs",
      globals: globals.node
    }
  }
];