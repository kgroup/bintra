/**
 * Some helper functions/utilities for replying back from API call.
 * @module utils/writer
 * @license MIT
 * @author Kai KRETSCHMANN <kai@kretschmann.consulting>
 */

/**
 * Set headers on HTTP level for replying.
 * @function setHeaders
 * @private
 * @param {object} response Object for response
 **/
function setHeaders (response) {
  response.setHeader('Cache-Control', 'max-age=0, no-store');
  response.setHeader('Pragma', 'no-cache');
  response.setHeader('Expires', '-1');
}

/**
 * Reply with content and numeric code into response object.
 * @function writeText
 * @public
 * @summary prepare text reply
 * @param {object} response Object for response
 * @param {object} payload Reply content, plain string
 * @param {number} code Reply code, numeric HTTP value
 **/
exports.writeText = function (response, payload, code) {
  setHeaders(response);
  response.writeHead(code, {
    'Content-Type': 'text/plain; charset=utf-8'
  });
  response.end(payload);
};

/**
 * Reply with content and numeric code into response object.
 * @function writeJson
 * @public
 * @summary prepare JSON reply
 * @param {object} response Object for response
 * @param {object} arg1 Reply content, either string or JSON
 * @param {number} arg2 Reply code, numeric HTTP value
 **/
exports.writeJson = function (response, arg1, arg2) {
  let payload = arg1;
  const code = arg2;

  if (typeof payload === 'object') {
    payload = JSON.stringify(payload, null, 2);
  }

  setHeaders(response);

  if (code >= 400) {
    response.writeHead(code, {
      'Content-Type': 'text/plain; charset=utf-8'
    });
  } else {
    response.writeHead(code, {
      'Content-Type': 'application/json'
    });
  }
  response.end(payload);
};
