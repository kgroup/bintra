/**
 * Some helper functions/utilities for event managing.
 * @module utils/eventer
 * @license MIT
 * @author Kai KRETSCHMANN <kai@kretschmann.consulting>
 */

const Emitter = require('events').EventEmitter;
const eventEmitter = new Emitter();

/**
 * Provide the event emitter object
 * @public
 * @exports eventEmitter
 **/
module.exports = {
  em: eventEmitter
};
