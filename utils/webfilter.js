'use strict';

/**
 * Some helper functions/utilities for web application filtering.
 * @module utils/webfilter
 * @license MIT
 * @author Kai KRETSCHMANN <kai@kretschmann.consulting>
 */

require('custom-env').env(true);

const log4js = require('log4js');
const logger = log4js.getLogger();
logger.level = process.env.LOGLEVEL || /* istanbul ignore next */ 'warn';

const pEnv = /\.env/g;
const pPhp = /\.php/g;
const pGit = /\.git/g;
const pWp = /wp-(admin|content|login)/g;

/**
 * Is the web filter convinced it was OK?
 * @method webFilterOK
 * @public
 * @summary check url
 * @param {object} req The web request object
 * @returns {boolean} return validity as boolean
 **/
exports.webFilterOK = function (req) {
  let u = req.originalUrl;
  if (u.indexOf('?') > -1) {
    logger.debug('Had to split argument off');
    u = u.split('?')[0];
  }
  logger.debug(u);

  if (u.startsWith('//')) return false;
  if (u.search(pEnv) > -1) return false;
  if (u.search(pPhp) > -1) return false;
  if (u.search(pGit) > -1) return false;
  if (u.search(pWp) > -1) return false;

  logger.debug('Query was clean');
  return true;
};
