'use strict';

/**
 * Some helper functions/utilities for waiting for startup.
 * @module utils/appwait
 * @license MIT
 * @author Kai KRETSCHMANN <kai@kretschmann.consulting>
 */

const myapp = require('../app').app;
const log4js = require('log4js');
const logger = log4js.getLogger();
logger.level = process.env.LOGLEVEL || /* istanbul ignore next */ 'warn';
const path = require('path');

/**
 * Wait for main app startup.
 * @method appWait
 * @public
 * @param {object} done Callback for event
 * @param {string} cfn The caller filename for logging
 **/
exports.appWait = function (done, cfn) {
  const callerFileName = path.basename(cfn);

  logger.warn(`Wait for app server start in ${callerFileName}`);
  if (myapp.didStart) {
    logger.info('app server already started');
    done();
  } else {
    logger.info('Not yet, wait for event');

    myapp.on('appStarted', function () {
      logger.info(`app server started now for ${callerFileName}`);
      done();
    });
  } // if
}
